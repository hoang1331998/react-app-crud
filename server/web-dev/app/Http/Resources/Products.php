<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Category;
use phpDocumentor\Reflection\Types\This;
use App\Http\Resources\Categories as CategoriesResource;

class Products extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' =>$this->name,
            'price' => $this->price,
            'image_path'=>$this->feature_image_path,
            'content' =>$this->content,
            'create_at' =>$this->created_at,
            'updated_at' =>$this->updated_at,
            'delete_at' => $this->delete_at,
            'category_id' => $this->category_id,
            'image_name'=> $this->feature_image_name,
            'category_name' =>$this->category['name'],
            //'category'=>new CategoriesResource($this->category),
            //'allCategory'=>Category::select('id')->get(),
        ];
    }
}
