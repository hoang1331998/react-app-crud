<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    // protected $fillable = [
    //     // 'name', 'price','category_id','content'
    // ];
    protected $guard = [];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

}

