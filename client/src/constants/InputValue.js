 const currencies = [
    {
      value: '1',
      label: 'Dog',
    },
    {
      value: '2',
      label: 'Cat',
    },
    {
      value: '3',
      label: 'Bird',
    },
    {
      value: '4',
      label: 'Mouse',
    },
  ];
export default currencies;