import axios from 'axios'

export function PostData(type, userData) {

    let BaseUrl = "http://localhost:8888/api/auth/";
    console.log(userData);

    return new Promise((resolve, reject) => {

        axios
        .post(BaseUrl + type, userData)
        .then((respone) => {
            resolve(respone);
        })
        .catch((error) => {
            reject(error)
        })     
    })
}