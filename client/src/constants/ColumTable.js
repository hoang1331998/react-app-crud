import { Button} from 'reactstrap';
import {Link} from 'react-router-dom';
import React from 'react';

const columns = [{
    dataField: 'id',
    text: ' ID',
    sort:true,
    headerStyle:() => {
      return {width:"5%"}
    }
  }, {
    dataField: 'name',
    text: 'Name',
    sort:true,
  }, {
    dataField: 'slug',
    text: 'Create',
    sort:true,
  },{
    dataField: 'updated_at',
    text: 'Update',
    sort:true,
  },
    {
    dataField: 'link',
    text: 'Action',
    headerStyle:() => {
      return {width:"25%"}
    },
    formatter: (rowContent, row) => {
      return(
        <div>
          <Link 
            to={`categories/edit`}
            color="success"
            className="btn btn-success btn-custom"
            >
            Edit
            </Link> {' '}
            <Button 
            
            color="danger"
            className="btn-custom"
            >
            Delete
            </Button>
        </div>
      );
    }
  
  }];

  export default columns;