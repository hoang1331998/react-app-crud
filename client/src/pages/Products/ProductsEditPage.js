import React, { Component } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import ProductForm from '../../components/Products/ProductForm';
import { putProductUpdate, getProductDetail } from '../../actions/productsAction';


const mapStateToProps = (state) => {
    return {
        getResponseEditProduct: state.products.getResponseEditProduct,
        errorResponseEditProduct: state.products.errorResponseEditProduct
    }
}

class ProductsEditPage extends Component {

    componentDidMount() {
        this.props.dispatch(getProductDetail(this.props.match.params.id));
    }

    handleSunmit(data) {
        console.log(data);
        this.props.dispatch(putProductUpdate(data, this.props.match.params.id));
    }
    render() {
        var success = this.props.getResponseEditProduct;
        var error = this.props.errorResponseEditProduct;
        console.log(success);
        if (success || error) {

            if (error) {
                swal("Update Faild !", error, "error");
            }
            else {
                swal("Update success !", "Username: " + success.name, "success");
            }
        }
        return (
            <div>
                <ProductForm onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps, null)(ProductsEditPage);
