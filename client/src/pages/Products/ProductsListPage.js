import React, { Component } from 'react';
import '../../assets/css/Categories.css'
import { connect } from 'react-redux';
import ProductTable from '../../components/Products/ProductTable';
import { Link } from 'react-router-dom';
import { getProductsList, deleteProduct, deleteDataProduct } from '../../actions/productsAction';


class ProductsListPage extends Component {

    componentDidMount() {
        this.props.dispatch(getProductsList())
        this.props.dispatch(deleteDataProduct())
    }


    handleDelete = async (id) => {
        
        try{
            await this.props.dispatch(deleteProduct(id))
            await this.props.dispatch(getProductsList())
        }
        catch(error){
            console.log(error);
        }    
    }
    render() {
        var { getProductsList } = this.props;
        return (

            <div className="contentPage">
                {getProductsList ?
                    <div>
                        <div className="content__title">
                            <div className="content__title--name">
                                <h4 >Products</h4>
                            </div>
                        </div>
                        <div className="content">
                            <div className="table-custom">
                                <div className="content__header">
                                    <h4 className="header-title">Products List </h4>
                                    <Link
                                        to="products/add"
                                        className="btn btn-info"
                                    >
                                        Insert
                            </Link>
                                </div>
                                <ProductTable
                                    getProductsList={getProductsList}
                                    handleDelete={this.handleDelete}
                                />
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        {this.props.errorProductsList ? <h3>{this.props.errorProductsList}</h3> : <h3>Loading...</h3>}
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        getProductsList: state.products.getProductsList,
        errorProductsList: state.products.errorProductsList
    }
}

export default connect(mapStateToProps, null)(ProductsListPage);
