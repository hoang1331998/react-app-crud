import React, { Component } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';

import { postProductCreate, getProductsList } from '../../actions/productsAction';
import ProductForm from '../../components/Products/ProductForm';

const mapStateToProps = (state) => {
    return {
        getResponseDataProduct: state.products.getResponseDataProduct,
        errorResponseDataProduct: state.products.errorResponseDataProduct
    }
}

class ProductsCreatePage extends Component {

    handleSunmit = async (data) => {      
        try{
            await this.props.dispatch(postProductCreate(data));
            await this.props.dispatch(getProductsList());  
        }
        catch(error){
            console.log(error);
        }    
    }

    render() {
        var success = this.props.getResponseDataProduct;
        var error = this.props.errorResponseDataProduct;
        if (success || error) {

            if (error) {
                swal("Create Faild !", error, "error");
            }
            else {
                swal("Create success !", "Username: " + success.name, "success");
            }
        }
        return (
            <div>
                <ProductForm onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps, null)(ProductsCreatePage);
