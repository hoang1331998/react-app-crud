import React, { Component } from 'react';
import LoginForm from '../../components/Login/LoginForm';

class LoginPage extends Component {
    render() {
        return (
            <div>
                <LoginForm/>
            </div>
        );
    }
}

export default LoginPage;
