import React, { Component } from 'react';
import FormComponent from '../../components/User/FormComponent';
import { postUserCreate } from '../../actions/userAction';
import { connect } from 'react-redux';

import swal from 'sweetalert';

const mapStateToProps = (state) => {
    return {
        getResponseDataUser: state.users.getResponseDataUser,
        errorResponseDataUser: state.users.errorResponseDataUser
    }
}

class UserCreatePage extends Component {


    handleSunmit(data) {
        this.props.dispatch(postUserCreate(data));
    }

    render() {
        var success = this.props.getResponseDataUser;
        var error = this.props.errorResponseDataUser;
        if (success || error) {

            if (error) {
                swal("Create Faild !", error, "error");
            }
            else {
                swal("Create success !", "Username: " + success.name, "success");
            }
        }
        return (
            <div>
                <FormComponent onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps, null)(UserCreatePage);
