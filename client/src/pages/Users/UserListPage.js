import React, { Component } from 'react';
import '../../assets/css/Categories.css'
import { connect } from 'react-redux';
import TableComponent from '../../components/User/TableComponent';
import { Link } from 'react-router-dom';
import { getUserList, deleteDataUser, deleteUser } from '../../actions/userAction';


class UserListPage extends Component {

    componentDidMount() {
        this.props.dispatch(getUserList())
        this.props.dispatch(deleteDataUser())
    }

    handleDelete = async (id) => {
        try{
            await this.props.dispatch(deleteUser(id))
            await this.props.dispatch(getUserList())
        }
        catch(error){
            console.log(error);
        }
        
    }
    render() {

        var { getUsersList } = this.props;
        return (
            <div className="contentPage">
                {getUsersList ?
                    <div>
                        <div className="content__title">
                            <div className="content__title--name">
                                <h4 >User</h4>
                            </div>
                        </div>
                        <div className="content">
                            <div className="table-custom">
                                <div className="content__header">
                                    <h4 className="header-title">Users List </h4>
                                    <Link
                                        to="users/add"
                                        className="btn btn-info"
                                    >
                                        Insert
                            </Link>
                                </div>
                                <TableComponent
                                    getUsersList={getUsersList}
                                    handleDelete={this.handleDelete}
                                />
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        {this.props.errorUsersList ? <h3>{this.props.errorUsersList}</h3> : <h3>Loading...</h3>}
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        getUsersList: state.users.getUsersList,
        errorUsersList: state.users.errorUsersList
    }
}

export default connect(mapStateToProps, null)(UserListPage);
