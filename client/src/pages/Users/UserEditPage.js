import React, { Component } from 'react';
import FormComponent from '../../components/User/FormComponent';
import { connect } from 'react-redux';

import swal from 'sweetalert';
import { getUserDetail, putUserUpdate } from '../../actions/userAction';


const mapStateToProps = (state) => {
    return {
        getResponseEditUser: state.users.getResponseEditUser,
        errorResponseEditUser: state.users.errorResponseEditUser
    }
}

class UserEditPage extends Component {

    componentDidMount(){
        this.props.dispatch(getUserDetail(this.props.match.params.id));
    }

    handleSunmit(data){
        this.props.dispatch(putUserUpdate(data,this.props.match.params.id));
    }
    render() {
        var success = this.props.getResponseEditUser;
        var error = this.props.errorResponseEditUser;
        console.log(success);
        if (success|| error) {

            if(error){
                swal("Update Faild !", error, "error");            
            }
            else{
                swal("Update success !", "Username: " + success.name, "success");
            }          
        }
        return (
            <div>
                <FormComponent onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps,null)(UserEditPage);
