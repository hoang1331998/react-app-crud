import React, { Component } from 'react';
import { connect } from 'react-redux';

import swal from 'sweetalert';
import { postCategoryCreate , getCategoriesList} from '../../actions/categoriesAction';
import CategoryForm from '../../components/Categories/CategoryForm';


const mapStateToProps = (state) => {
    return {
        getResponseDataCategory: state.categories.getResponseDataCategory,
        errorResponseDataCategory: state.categories.errorResponseDataCategory,
        getCategoriesList: state.categories.getCategoriesList
    }
}

class CategoriesCreatePage extends Component {

    handleSunmit = async(data) => {
        try{
            await this.props.dispatch(postCategoryCreate(data))
            await this.props.dispatch(getCategoriesList());  
        }
        catch(error){
            console.log(error);
        }     
    }

    render() {

        var success = this.props.getResponseDataCategory;
        var error = this.props.errorResponseDataCategory;

        if (success || error) {

            if (error) {
                swal("Create Faild !", error, "error");
            }
            else {
                swal("Create success !", "Username: " + this.props.getResponseDataCategory.name, "success");
            }
        }
        return (
            <div>
                <CategoryForm onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps, null)(CategoriesCreatePage);
