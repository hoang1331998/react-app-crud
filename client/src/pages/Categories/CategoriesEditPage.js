import React, { Component } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { getCategoryDetail, putCategoryUpdate } from '../../actions/categoriesAction';
import CategoryForm from '../../components/Categories/CategoryForm';


const mapStateToProps = (state) => {
    return {
        getResponseEditCategory: state.categories.getResponseEditCategory,
        errorResponseEditCategory: state.categories.errorResponseEditCategory
    }
}

class CategoriesEditPage extends Component {

    componentDidMount(){
        this.props.dispatch(getCategoryDetail(this.props.match.params.id));
    }

    handleSunmit(data){
        console.log(data);
        this.props.dispatch(putCategoryUpdate(data,this.props.match.params.id));
    }
    render() {
        var success = this.props.getResponseEditCategory;
        var error = this.props.errorResponseEditCategory;
        console.log(success);
        if (success|| error) {

            if(error){
                swal("Update Faild !", this.props.errorResponseEditCategory, "error");            
            }
            else{
                swal("Update success !", "Username: " + this.props.getResponseEditCategory.name, "success");
            }          
        }
        return (
            <div>
                <CategoryForm onSubmit={(data) => this.handleSunmit(data)} />
            </div>
        );
    }
}

export default connect(mapStateToProps,null)(CategoriesEditPage);
