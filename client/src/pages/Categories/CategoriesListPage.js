import React, { Component } from 'react';
import '../../assets/css/Categories.css'
import { connect } from 'react-redux';
import CategoryTable from '../../components/Categories/CategoryTable';
import { Link } from 'react-router-dom';
import { getCategoriesList, deleteCategory, deleteDataCategory } from '../../actions/categoriesAction';


class CategoryListPage extends Component {

    componentWillMount() {
        this.props.dispatch(getCategoriesList())
        this.props.dispatch(deleteDataCategory())
    }

    handleDelete = async (id) => {
        try{
            await this.props.dispatch(deleteCategory(id))
            await this.props. dispatch(getCategoriesList())
        }
        catch(error){
            console.log(error);
        } 
    }
    render() {
        var { getCategoriesList } = this.props;
        return (

            <div className="contentPage">
                {getCategoriesList ?
                    <div>
                        <div className="content__title">
                            <div className="content__title--name">
                                <h4 >Categories</h4>

                            </div>
                        </div>
                        <div className="content">
                            <div className="table-custom">
                                <div className="content__header">
                                    <h4 className="header-title">Categories List </h4>
                                    <Link
                                        to="categories/add"
                                        className="btn btn-info"
                                    >
                                        Insert
                            </Link>
                                </div>
                                <CategoryTable 
                                getCategoriesList={getCategoriesList} 
                                handleDelete = {this.handleDelete}
                                />
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        {this.props.errorCategoriesList ? <h3>{this.props.errorCategoriesList}</h3> : <h3>Loading...</h3>}
                    </div>
                    }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        getCategoriesList: state.categories.getCategoriesList,
        errorCategoriesList: state.categories.errorCategoriesList,
    }
}

export default connect(mapStateToProps, null)(CategoryListPage);
