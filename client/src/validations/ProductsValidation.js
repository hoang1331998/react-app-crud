export const ProductsValidation = (values) => {
    const errors = {};

    if(!values.name || values.name === ""){
        errors.name = "Name is required !!!";
    }
    if(!values.price || values.price === ""){
        errors.price = "Price is required !!!";
    }
    if(!values.category_id || values.category_id === ""){
        errors.category_id = "Category is required !!!";
    }

    return errors;
}