export const CategoriesValidations = (values) => {
    const errors = {};

    if(!values.name || values.name === ""){
        errors.name = "Name is required !!!";
    }
    if(!values.parent_id || values.parent_id === ""){
        errors.parent_id = "Parent is required !!!";
    }
    if(!values.slug || values.slug === ""){
        errors.slug = "Slug is required !!!";
    }

    return errors;
}