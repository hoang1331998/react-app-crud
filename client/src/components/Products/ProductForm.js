import React, { Component } from 'react';
import '../../assets/css/CategoriesForm.css';
import { Button, FormGroup, Label } from 'reactstrap';
import { connect } from 'react-redux';
import { reduxForm, Field } from "redux-form";
import { ProductsValidation } from '../../validations/ProductsValidation';
import { Link } from 'react-router-dom';

import Axios from 'axios';
import {renderField, renderSelectField} from '../Field/RenderField';

const mapStateToProps = (state) => ({

    initialValues: {
        name: state.products.getProductDetail.name,
        category_id: state.products.getProductDetail.category_id,
        price: state.products.getProductDetail.price,
        content: state.products.getProductDetail.content,
    }
})

class ProductForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: []
        }
    }

    componentDidMount() {
        Axios.get("http://localhost:8888/api/categories").then(res => {
            let category = res.data.data
            this.setState({ category })
        })
    }
    render() {
        var { category } = this.state
        return (
            <div>
                <div className="contentPage">
                    <div className="content__title">
                        <div className="content__title--name">
                            <h4 >Add Product</h4>
                        </div>
                    </div>

                    <form
                        className="formCustom"
                        onSubmit={this.props.handleSubmit}
                    >
                        <div className="contentForm">
                            <FormGroup >
                                <Label htmlFor="name" className="col-form-label">
                                    Name
                                </Label>
                                <Field
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="name"
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup >
                                <Field width="100%"
                                    id="outlined-textarea"
                                    component={renderSelectField}
                                    name="category_id"
                                    type="select"
                                >
                                    <option></option>
                                    {category.length > 0 ? category.map(item =>
                                        <option key={item.key} value={item.id}>{item.name}</option>
                                    ) : ''};
                                </Field>
                            </FormGroup>
                            <FormGroup >
                                <Label htmlFor="price" className="col-form-label">
                                    Price
                                </Label>
                                <Field width="100%"
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="price"
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup >
                                <Label htmlFor="content" className="col-form-label">
                                    Content
                                </Label>
                                <Field width="100%"
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="content"
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Link className="btn btn-secondary btn-custom" to="/products">Back</Link> {' '}
                                <Button
                                    type="submit"
                                    className="btn-custom"
                                    disabled={this.props.submitting}
                                    color="primary">Save</Button>
                            </FormGroup>
                        </div>

                    </form>

                </div>
            </div>
        );
    }
}

ProductForm = reduxForm({
    form: "formActionProducts",
    validate: ProductsValidation,
    enableReinitialize: true,
})(ProductForm);
export default connect(mapStateToProps, null)(ProductForm);
