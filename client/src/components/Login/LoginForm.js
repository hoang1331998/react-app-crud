import React, { Component } from 'react';
import '../../assets/css/Login.css';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';
import FacebookIcon from '@material-ui/icons/Facebook';
import Fab from '@material-ui/core/Fab';
import TwitterIcon from '@material-ui/icons/Twitter';

import { Field, reduxForm } from 'redux-form'
import {PostData} from '../../constants/PostData';
import {Redirect} from 'react-router-dom';
import {validate} from '../../validations/LoginValidation';
import {renderTextField} from '../../constants/RenderField';

class LoginForm extends Component {

    constructor(props){
        super(props);
        this.state={
            email:null,
            password:null,
            redirect: false,
            validate:false
        }
    }

    handleSubmit = (e) =>{

        e.preventDefault(); 

        if(this.state.email && this.state.password){
            PostData('login',this.state)
            .then((results) => {

                let responseJSON = results;
                console.log(responseJSON);

                if(responseJSON.data){

                    localStorage.setItem('userData',responseJSON.data)
                    this.setState({redirect:true})
                    console.log(localStorage.key);
                }
                else{
                    console.log("Login error");            
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({validate:true})
            })
        }
       
    }
    onChange = (e) =>{
        this.setState({[e.target.name]:e.target.value})
    }
    render() {

        if(this.state.redirect){
            return(<Redirect to="/"/>)
        }
        if(localStorage.getItem("userData")){
            return(<Redirect to="/"/>)
        }
        return (
            <div className="loginPage">
                <div className="loginPage__wrapper">
                    <div className="loginPage__content">
                        <div className="loginPage__form">
                            <form >
                                <div className="lp__row--header">
                                    <h2>Login Page</h2>
                                </div>
                                <div className="lp__row">
                                    <h4>Login into your account</h4>
                                </div>
                                <div className="lp__row">
                                {this.state.validate === true ? <p style={{color: "red"}} >Error ! Username or password is not exist</p> : ''}
                                </div>                         
                                <div className="lp__row">
                                    <Field
                                        onChange={this.onChange}
                                        name="email"
                                        component={renderTextField}
                                        label="Email"
                                    />
                                </div>
                                <div className="lp__row">
                                    <Field
                                        onChange={this.onChange}
                                        name="password"
                                        component={renderTextField}
                                        label="Password"
                                        type="password"
                                    />
                                </div>
                                <div className="lp__row">
                                    <div className="lp__row--checkbox">
                                        <div className="custom-checkbox">
                                            <Checkbox
                                                checked
                                                className="custom-checkbox"
                                                color="primary"
                                                inputProps={{ 'aria-label': 'secondary checkbox' }}
                                            />
                                        </div>
                                        <p className="rememberMe">Remember me</p>
                                    </div>
                                    <button
                                     onClick={this.handleSubmit} 
                                     disabled={this.props.submitting}
                                     className="btn btn-outline-primary" >Login</button>
                                </div>
                                <div className="text-center">
                                    <p>Or login with</p>
                                </div>
                                <div className="lp__row--bot">
                                    <div className="item">
                                        <Tooltip title="Add" aria-label="add">
                                            <Fab color="primary" >
                                                <FacebookIcon />
                                            </Fab>
                                        </Tooltip>
                                    </div>
                                    <div className="item">
                                        <Tooltip title="Add" aria-label="add">
                                            <Fab color="primary" >
                                                <TwitterIcon />
                                            </Fab>
                                        </Tooltip>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



export default reduxForm({
    form: 'LoginForm', // a unique identifier for this form
    validate,
})(LoginForm);
