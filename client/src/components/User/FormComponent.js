import React, { Component } from 'react';
import '../../assets/css/CategoriesForm.css';
import { Button, FormGroup, Input, Label } from 'reactstrap';
import { connect } from 'react-redux';
import { reduxForm, Field } from "redux-form";
import { UserValidations } from '../../validations/UserValidations';
import { Link } from 'react-router-dom';



const renderField = ({
    input,
    type,
    placeholder,
    disabled,
    readOnly,
    meta: { touched, error, warning },
}) => (
        <div>
            
            <Input
                {...input}
                type={type}
                placeholder={placeholder}
                disabled={disabled}
                readOnly={readOnly}
            />
            {touched &&
                ((error && <p style={{ color: "red" }}>{error}</p>)) ||
                ((warning && <p style={{ color: "brown" }}>{warning} </p>))
            }
        </div>
    );

const mapStateToProps = (state) => ({
    initialValues: {
        name: state.users.getUsersDetail.name,
        email: state.users.getUsersDetail.email,
        password: state.users.getUsersDetail.password
    }
})
class FormComponent extends Component {
    render() {
        return (
            <div>
                <div className="contentPage">
                    <div className="content__title">
                        <div className="content__title--name">
                            <h4 >Add Category</h4>
                        </div>
                    </div>

                    <form
                        className="formCustom"
                        onSubmit={this.props.handleSubmit}
                    >
                        <div className="contentForm">
                            <FormGroup >
                                <Label htmlFor="name" className="col-form-label">
                                    Name   
                                </Label>
                                <Field
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="name"
                                    lanel="Name "
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup >
                                <Label htmlFor="email" className="col-form-label">
                                    Email  
                                </Label>
                                <Field  width="100%"
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="email"
                                    lanel="Email "
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Link className="btn btn-secondary btn-custom" to="/users">Back</Link> {' '}
                                <Button
                                    type="submit"
                                    className="btn-custom"
                                    disabled={this.props.submitting}
                                    color="primary">Save</Button>
                            </FormGroup>
                        </div>

                    </form>

                </div>
            </div>
        );
    }
}

FormComponent = reduxForm({
    form: "formActionUser",
    validate: UserValidations,
    enableReinitialize: true,
})(FormComponent);
export default connect(mapStateToProps,null)(FormComponent);
