import React, { Component } from 'react';
import '../../assets/css/CategoriesForm.css';
import { Button, FormGroup, Label } from 'reactstrap';
import { connect } from 'react-redux';
import { reduxForm, Field } from "redux-form";
import { CategoriesValidations } from '../../validations/CategoriesValidations';
import { Link } from 'react-router-dom';
import {renderField} from '../Field/RenderField';



const mapStateToProps = (state) => ({
    initialValues: {
        name: state.categories.getCategoriesDetail.name,
        parent_id: state.categories.getCategoriesDetail.parent_id,
        slug: state.categories.getCategoriesDetail.slug
    }
})
class CategoryForm extends Component {
    render() {
        return (
            <div>
                <div className="contentPage">
                    <div className="content__title">
                        <div className="content__title--name">
                            <h4 >Add Category</h4>
                        </div>
                    </div>

                    <form
                        className="formCustom"
                        onSubmit={this.props.handleSubmit}
                    >
                        <div className="contentForm">
                            <FormGroup >
                                <Label htmlFor="name" className="col-form-label">
                                    Name
                                </Label>
                                <Field
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="name"
                                    lanel="Name "
                                    type="text"
                                />
                            </FormGroup>
                            <FormGroup >
                                <Label htmlFor="parent_id" className="col-form-label">
                                    Category Parent
                                </Label>
                                <Field width="100%"
                                    id="outlined-textarea"
                                    component={renderField}
                                    name="parent_id"
                                    lanel="Parent "
                                    type="text"
                                />
                            </FormGroup>                       
                            <FormGroup>
                                <Link className="btn btn-secondary btn-custom" to="/categories">Back</Link> {' '}
                                <Button
                                    type="submit"
                                    className="btn-custom"
                                    disabled={this.props.submitting}
                                    color="primary">Save</Button>
                            </FormGroup>
                        </div>

                    </form>

                </div>
            </div>
        );
    }
}

CategoryForm = reduxForm({
    form: "formActionCategories",
    validate: CategoriesValidations,
    enableReinitialize: true,
})(CategoryForm);
export default connect(mapStateToProps, null)(CategoryForm);
