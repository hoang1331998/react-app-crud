import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import swal from 'sweetalert';

const { SearchBar } = Search;

const defaultSorted = [{
  dataField: 'id',
  order: 'desc'
}];
const options = {
  paginationSize: 4,
  pageStartIndex: 1,
  firstPageText: 'First',
  prePageText: 'Back',
  nextPageText: 'Next',
  lastPageText: 'Last',
  nextPageTitle: 'First page',
  prePageTitle: 'Pre page',
  firstPageTitle: 'Next page',
  lastPageTitle: 'Last page',
  disablePageTitle: true,
  sizePerPageList: [{
    text: '5', value: 5
  }, {
    text: '10', value: 10
  },]
};

const CategoryTable = (props) => {

  const handleDelete = (id) => {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          props.handleDelete(id)
          swal("Your imaginary file has been deleted!", {
            icon: "success",
          });
        } else {
          swal("Your imaginary file is safe!");
        }
      });
  }
  const columns = [{
    dataField: 'id',
    text: ' ID',
    sort: true,
    headerStyle: () => {
      return { width: "5%" }
    }
  }, {
    dataField: 'name',
    text: 'Name',
    sort: true,
  }, {
    dataField: 'parent_id',
    text: 'Parent',
    sort: true,
  },
  {
    dataField: 'link',
    text: 'Action',
    headerStyle: () => {
      return { width: "20%" }
    },
    formatter: (rowContent, row) => {
      return (
        <div>
          <Link
            to={`categories/${row.id}/edit`}
            color="success"
            className="btn btn-success btn-custom"
          >
            Edit
            </Link> {' '}
          <Button

            color="danger"
            className="btn-custom"
            onClick={() => handleDelete(row.id)}
          >
            Delete
            </Button>
        </div>
      );
    }

  }];

  return (
    <div>
      <ToolkitProvider
        bootstrap4
        keyField='id'
        data={props.getCategoriesList}
        columns={columns}
        search
      >
        {
          props => (
            <div>
              <SearchBar {...props.searchProps} />
              <BootstrapTable
                defaultSorted={defaultSorted}
                {...props.baseProps}
                pagination={paginationFactory(options)}
              />
            </div>
          )
        }
      </ToolkitProvider >
    </div>
  )
};



export default CategoryTable;