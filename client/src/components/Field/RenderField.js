import React from 'react';
import {  Input} from 'reactstrap';
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'

export const renderField = ({
    input,
    type,
    placeholder,
    disabled,
    readOnly,
    meta: { touched, error, warning },
}) => (
        <div>

            <Input
                {...input}
                type={type}
                placeholder={placeholder}
                disabled={disabled}
                readOnly={readOnly}
            />
            {touched &&
                ((error && <p style={{ color: "red" }}>{error}</p>)) ||
                ((warning && <p style={{ color: "brown" }}>{warning} </p>))
            }
        </div>
    );

export const renderSelectField = ({
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
}) => (
        <FormControl error={touched && error}>
            <InputLabel htmlFor="category_id">Category</InputLabel>
            <Select
                native
                {...input}
                {...custom}
                inputProps={{
                    name: 'category_id',
                    id: 'category_id'
                }}
            >
                {children}
            </Select>
            {renderFromHelper({ touched, error })}
        </FormControl>
    )
const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
        return
    } else {
        return <FormHelperText>{touched && error}</FormHelperText>
    }
}