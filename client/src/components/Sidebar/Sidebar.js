import React, { Component } from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import '../../assets/css/Sidebar.css';
import GroupIcon from '@material-ui/icons/Group';
import { Link } from "react-router-dom";
import CategoryIcon from '@material-ui/icons/Category';
import LocalPizzaIcon from '@material-ui/icons/LocalPizza';
import { Redirect } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import avatar from '../../assets/images/avatar.jpg'

class Sidebar extends Component {
    constructor(props){

        super(props)
        this.state={
            redirect: false,
            anchorEl:null
        }
    }
   // const [anchorEl, setAnchorEl] = React.useState(null);

    handleClick = (event) => {
        //setAnchorEl(event.currentTarget);
        this.setState({anchorEl:event.currentTarget})
    };
    componentWillMount(){
        var user = '';
        if(user=localStorage.getItem("userData")){
            console.log("Login success",user);
        }
        else{
           this.setState({redirect:true})
        }
    }
    handleClose = () => {
        this.setState({anchorEl:null})
    };
    logout = ()=>{
        localStorage.clear()
        this.setState({redirect: true})
    }
    render(){
        if(this.state.redirect){
            return<Redirect to="/login"/>
        }
        return (
            <div className="sideBar">
                <div className="userProfile">
                    <img className="avatar" src={avatar} alt="avatar" />
                    <div className="userInfo">
                        <h4 className="userInfo__name">Hoang Vu</h4>
                        <p className="userInfo__role">Adminstrator</p>
                    </div>
                    <div className="userInfo__dropdown">
                        <div>
                            <ExpandMoreIcon aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick}>
                                Open Menu
                            </ExpandMoreIcon>
                            <Menu
                                id="simple-menu"
                                anchorEl={this.state.anchorEl}
                                keepMounted
                                open={Boolean(this.state.anchorEl)}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                <MenuItem onClick={this.handleClose}>My account</MenuItem>
                                <MenuItem onClick={this.logout}>Logout</MenuItem>
                            </Menu>
                        </div>
                    </div>
                </div>
                <div className="sideMenu">
                    <Link to="/users">
                        <div className="sideMenu__row">
                            <div className="sideMenu__row--icon">
                                <GroupIcon />
                            </div>
                        Users
                    </div>
                    </Link>
                    <Link to="/categories">
                        <div className="sideMenu__row">
                            <div className="sideMenu__row--icon">
                                <CategoryIcon />
                            </div>
                        Categories
                    </div>
                    </Link>
                    <Link to="/products">
                        <div className="sideMenu__row">
                            <div className="sideMenu__row--icon">
                                <LocalPizzaIcon />
                            </div>
                        Product
                    </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default Sidebar;
