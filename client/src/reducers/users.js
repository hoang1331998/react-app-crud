import { GET_USER_LIST, POST_USER_CREATE, GET_USER_DETAIL, PUT_USER_EDIT, DELETE_USER } from '../actions/userAction'


var initialState = {
    getUsersList: false,
    errorUsersList: false,

    getUsersDetail: false,
    errorUsersDetail: false,

    getResponseDataUser: false,
    errorResponseDataUser: false,

    getResponseEditUser: false,
    errorResponseEditUser: false,

    getResponseDeleteUser: false,
    errorResponseDeleteUser: false,
    title: "Ahihi"
};

const users = (state = initialState, action) => {

    switch (action.type) {
        case GET_USER_LIST:
            return {
                ...state,
                getUsersList: action.payload.data,
                errorUserList: action.payload.errorMessage
            }
        case GET_USER_DETAIL:
            return {
                ...state,
                getUsersDetail: action.payload.data,
                errorUsersDetail: action.payload.errorMessage
            }
        case POST_USER_CREATE:
            return {
                ...state,
                getResponseDataUser: action.payload.data,
                errorResponseDataUser: action.payload.errorMessage
            }
        case PUT_USER_EDIT:

            return {
                ...state,
                getResponseEditUser: action.payload.data,
                errorResponseEditUser: action.payload.errorMessage
            }
        case DELETE_USER:
            console.log("Id remove:", action.payload.data)
            return {
                ...state,
                getResponseDeleteUser: action.payload.data,
                errorResponseDeleteUser: action.payload.errorMessage
            }
        default:
            return state;
    }
}
export default users;