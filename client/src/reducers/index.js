import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import users from './users';
import categories from './categories';
import products from './products';

export default combineReducers({
    users,
    categories,
    products,
    form: formReducer,

});

