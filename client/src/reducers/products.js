import { GET_PRODUCTS_LIST, GET_PRODUCT_DETAIL, POST_PRODUCT_CREATE, PUT_PRODUCT_EDIT, DELETE_PRODUCT } from '../actions/productsAction'


var initialState = {
    getProductsList: false,
    errorProductsList: false,
    getCategoryList: false,

    getProductDetail: false,
    errorProductDetail: false,

    getResponseDataProduct: false,
    errorResponseDataProduct: false,

    getResponseEditProduct: false,
    errorResponseEditProduct: false,

    getResponseDeleteProduct: false,
    errorResponseDeleteProduct: false,
    title: "Ahihi"
};

const products = (state = initialState, action) => {

    switch (action.type) {
        case GET_PRODUCTS_LIST:
            return {
                ...state,
                getProductsList: action.payload.data,
                errorProductsList: action.payload.errorMessage,
                getCategoryList: action.payload.categoryList
            }
        case GET_PRODUCT_DETAIL:
            return {
                ...state,
                getProductDetail: action.payload.data,
                errorProductDetail: action.payload.errorMessage
            }
        case POST_PRODUCT_CREATE:
            return {
                ...state,
                getResponseDataProduct: action.payload.data,
                errorResponseDataProduct: action.payload.errorMessage
            }
        case PUT_PRODUCT_EDIT:

            return {
                ...state,
                getResponseEditProduct: action.payload.data,
                errorResponseEditProduct: action.payload.errorMessage
            }
        case DELETE_PRODUCT:

            return {
                ...state,
                getResponseDeleteProduct: action.payload.data,
                errorResponseDeleteProduct: action.payload.errorMessage
            }
        default:
            return state;
    }
}
export default products;