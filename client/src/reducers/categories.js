import { GET_CATEGORIES_LIST, GET_CATEGORY_DETAIL, POST_CATEGORY_CREATE, PUT_CATEGORY_EDIT, DELETE_CATEGORY } from '../actions/categoriesAction'


var initialState = {
    getCategoriesList: false,
    errorCategoriesList: false,

    getCategoriesDetail: false,
    errorCategoriesDetail: false,

    getResponseDataCategory: false,
    errorResponseDataCategory: false,

    getResponseEditCategory: false,
    errorResponseEditCategory: false,

    getResponseDeleteCategory: false,
    errorResponseDeleteCategory: false,
};

const categories = (state = initialState, action) => {

    switch (action.type) {
        case GET_CATEGORIES_LIST:
            return {
                ...state,
                getCategoriesList: action.payload.data,
                errorCategoriesList: action.payload.errorMessage
            }
        case GET_CATEGORY_DETAIL:
            return {
                ...state,
                getCategoriesDetail: action.payload.data,
                errorCategoriesDetail: action.payload.errorMessage
            }
        case POST_CATEGORY_CREATE:
            return {
                ...state,
                getResponseDataCategory: action.payload.data,
                errorResponseDataCategory: action.payload.errorMessage
            }
        case PUT_CATEGORY_EDIT:

            return {
                ...state,
                getResponseEditCategory: action.payload.data,
                errorResponseEditCategory: action.payload.errorMessage
            }
        case DELETE_CATEGORY:
                    
            return {
                ...state,
                getResponseDeleteCategory: action.payload.data,
                errorResponseDeleteCategory: action.payload.errorMessage
            }
        default:
            return state;
    }
}
export default categories;