import axios from 'axios'

export const GET_CATEGORIES_LIST = "GET_CATEGORIES_LIST";
export const GET_CATEGORY_DETAIL = "GET_CATEGORY_DETAIL";
export const POST_CATEGORY_CREATE = "POST_CATEGORY_CREATE";
export const PUT_CATEGORY_EDIT = "PUT_CATEGORY_EDIT";
export const DELETE_CATEGORY = "DELETE_CATEGORY";

const url = "http://localhost:8888/api/categories/";

export const postCategoryCreate = (data) => {
    
    return async (dispatch) => {
        await axios.post(url,data)
        .then(function(res){
            let results = res.data.data;
            dispatch({
                type: POST_CATEGORY_CREATE,
                payload: {
                    data: results,
                    errorMessage: false,                 
                }              
            })
        })        
        .catch(function(err){
            dispatch({
                type: POST_CATEGORY_CREATE,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

export const getCategoriesList = () => {
  
    return async (dispatch) => {
        
        await axios.get(url)
        .then(function(res){
            let results = res.data.data;        
            dispatch({
                type: GET_CATEGORIES_LIST,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_CATEGORIES_LIST,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const getCategoryDetail = (id) => {
  
    return async(dispatch) => {
        await axios.get(url + id)
        .then(function(res){
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: GET_CATEGORY_DETAIL,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_CATEGORY_DETAIL,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};




export const putCategoryUpdate = (data, id) => {

    return async (dispatch) => {
        await axios.put(url + id,data)
        .then(function(res){
            console.log(res)
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: PUT_CATEGORY_EDIT,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: PUT_CATEGORY_EDIT,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

//////////Delete user ------------------
export const deleteCategory = (id) => {
  
    return async (dispatch) => {
        await axios.delete(url + id)
        .then(function(res){       
            console.log(res); 
            dispatch({
                type: DELETE_CATEGORY,
                payload: {
                    data:id,
                    errorMessage: false
                }
            })      
        })
        .catch(function(err){
            console.log(err);  
            dispatch({
                type: DELETE_CATEGORY,
                payload:{
                    errorMessage: err.message
                }
            })
        })
    }
};


/////////Delete cache data---------------
export const deleteDataCategory = () =>{

    return async (dispatch) => {
        
        await dispatch({
            type: GET_CATEGORY_DETAIL,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        await dispatch({
            type: POST_CATEGORY_CREATE,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        await dispatch({
            type: PUT_CATEGORY_EDIT,
            payload: {
                data: false,
                errorMessage: false
            }
        })
    }
}