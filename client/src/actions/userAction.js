import axios from 'axios'

export const GET_USER_LIST = "GET_USER_LIST";
export const GET_USER_DETAIL = "GET_USER_DETAIL";
export const POST_USER_CREATE = "POST_USER_CREATE";
export const PUT_USER_EDIT = "PUT_USER_EDIT";
export const DELETE_USER = "DELETE_USER";

const url = "http://localhost:8888/api/users/";

export const getUserList = () => {
  
    return async (dispatch) => {
        await axios.get(url)
        .then(function(res){
            let results = res.data.data;        
            dispatch({
                type: GET_USER_LIST,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_USER_LIST,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const getUserDetail = (id) => {
  
    return async (dispatch) => {
        await axios.get(url + id)
        .then(function(res){
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: GET_USER_DETAIL,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_USER_DETAIL,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

export const postUserCreate = (data) => {
  
    return async (dispatch) => {
        await axios.post(url,data)
        .then(function(res){
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: POST_USER_CREATE,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: POST_USER_CREATE,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const putUserUpdate = (data, id) => {
  
    return async(dispatch) => {
        await axios.put(url + id,data)
        .then(function(res){
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: PUT_USER_EDIT,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: PUT_USER_EDIT,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

//////////Delete user ------------------
export const deleteUser = (id) => {
  
    return async (dispatch) => {
        await axios.delete(url + id)
        .then(function(res){       
            console.log(res); 
            dispatch({
                type: DELETE_USER,
                payload: {
                    data:id,
                    errorMessage: false
                }
            })      
        })
        .catch(function(err){
            console.log(err);  
            dispatch({
                type: DELETE_USER,
                payload:{
                    errorMessage: err.message
                }
            })
        })
    }
};


/////////Delete cache data---------------
export const deleteDataUser = () =>{
    return async(dispatch) => {
        await dispatch({
            type: GET_USER_DETAIL,
            payload: {
                data: false,
                errorMessage: false
            }
        })

        await dispatch({
            type: POST_USER_CREATE,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        await dispatch({
            type: PUT_USER_EDIT,
            payload: {
                data: false,
                errorMessage: false
            }
        })
    }
}