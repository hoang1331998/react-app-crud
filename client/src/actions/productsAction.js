import axios from 'axios'

export const GET_PRODUCTS_LIST = "GET_PRODUCTS_LIST";
export const GET_PRODUCT_DETAIL = "GET_PRODUCT_DETAIL";
export const POST_PRODUCT_CREATE = "POST_PRODUCT_CREATE";
export const PUT_PRODUCT_EDIT = "PUT_PRODUCT_EDIT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

const url = "http://localhost:8888/api/products/";

export const getProductsList = () => {
  
    return async(dispatch) => {
        
        await axios.get(url)
        .then(function(res){
            let results = res.data.data;     
            dispatch({
                type: GET_PRODUCTS_LIST,
                payload: {
                    data: results,
                    errorMessage: false,
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_PRODUCTS_LIST,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const getProductDetail = (id) => {
  
    return async(dispatch) => {
        await axios.get(url + id)
        .then(function(res){
            let results = res.data.data;
            dispatch({
                type: GET_PRODUCT_DETAIL,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_PRODUCT_DETAIL,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

export const postProductCreate = (data) => {
    
    return async (dispatch) => {
        await axios.post(url,data)
        .then(function(res){
            let results = res.data.data;
            dispatch({
                type: POST_PRODUCT_CREATE,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: POST_PRODUCT_CREATE,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const putProductUpdate = (data, id) => {

    return async (dispatch) => {
        await axios.put(url + id,data)
        .then(function(res){
            let results = res.data.data;
            dispatch({
                type: PUT_PRODUCT_EDIT,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: PUT_PRODUCT_EDIT,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

//////////Delete user ------------------
export const deleteProduct = (id) => {
  
    return async(dispatch) => {
        await axios.delete(url + id)
        .then(function(res){       
            console.log(res); 
            dispatch({
                type: DELETE_PRODUCT,
                payload: {
                    data:id,
                    errorMessage: false
                }
            })      
        })
        .catch(function(err){
            console.log(err);  
            dispatch({
                type: DELETE_PRODUCT,
                payload:{
                    errorMessage: err.message
                }
            })
        })
    }
};


/////////Delete cache data---------------
export const deleteDataProduct = () =>{

    return async(dispatch) => {
        
        await dispatch({
            type: GET_PRODUCT_DETAIL,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        await dispatch({
            type: POST_PRODUCT_CREATE,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        await dispatch({
            type: PUT_PRODUCT_EDIT,
            payload: {
                data: false,
                errorMessage: false
            }
        })
    }
}