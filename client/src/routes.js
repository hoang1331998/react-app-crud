import React from 'react';
import NotFound from './pages/NotFound/NotFound';
import UserListPage from './pages/Users/UserListPage';
import UserCreatePage from './pages/Users/UserCreatePage';
import UserEditPage from './pages/Users/UserEditPage';
import CategoriesListPage from './pages/Categories/CategoriesListPage';
import CategoriesCreatePage from './pages/Categories/CategoriesCreatePage';
import CategoriesEditPage from './pages/Categories/CategoriesEditPage';
import ProductsListPage from './pages/Products/ProductsListPage';
import ProductsCreatePage from './pages/Products/ProductsCreatePage';
import ProductsEditPage from './pages/Products/ProductsEditPage';
import Home from './pages/Home/Home';

const routes = [
    {
        path: '/',
        exact: true,
        main: () => <Home/>
    },
    {
        path: '/users',
        exact: true,
        main: () => <UserListPage />
    },
    {
        path: '/users/add',
        exact: true,
        main: () => <UserCreatePage />
    },
    {
        path: '/users/:id/edit',
        exact: true,
        main: ({ match }) => <UserEditPage match={match} />
    },
    {
        path: '/categories',
        exact: true,
        main: () => <CategoriesListPage />
    },
    {
        path: '/categories/add',
        exact: true,
        main: () => <CategoriesCreatePage />
    },
    {
        path: '/categories/:id/edit',
        exact: true,
        main: ({ match }) => <CategoriesEditPage match={match} />
    },
    {
        path: '/products',
        exact: true,
        main: () => <ProductsListPage />
    },
    {
        path: '/products/add',
        exact: true,
        main: () => <ProductsCreatePage />
    },
    {
        path: '/products/:id/edit',
        exact: true,
        main: ({ match }) => <ProductsEditPage match={match} />
    },
    {
        path: '',
        exact: true,
        main: () => <NotFound />
    },

];

export default routes;