import React, { Component } from 'react';
import Navbar from './components/Navbar/Navbar';
import Sidebar from './components/Sidebar/Sidebar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import routes from './routes';
import LoginPage from './pages/Auth/LoginPage';


class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={LoginPage} />
          <div className="App">
            <Navbar />
            <Sidebar />
            {this.showMenu(routes)}
          </div>
        </Switch>
      </Router>
    );
  }
  showMenu = () => {
    var results = null;
    if (routes.length > 0) {
      results = routes.map((route, index) => {
        return <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      })
    }
    return <Switch>{results}</Switch>
  }

}

export default App;
